package com.jl.controller;

import com.jl.dao.SignDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/sign.do")
public class SignController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码防止乱码
        req.setCharacterEncoding("utf8");
        resp.setContentType("application/json;charset=utf-8");

        //从index异步获取值
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        SignDao signDao = new SignDao();
        try {
            int i = signDao.saveCustormer(username, password, email);
            if ( i == 1){
                resp.getWriter().write("插入成功!");
                resp.setHeader("Refresh", "3;/Login.html");
            }else {
                resp.getWriter().write("失败......");
                resp.setHeader("Refresh","3;/index.html");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
