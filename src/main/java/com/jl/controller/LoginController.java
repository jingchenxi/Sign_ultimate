package com.jl.controller;

import com.jl.bean.CustormerBean;
import com.jl.dao.LoginDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login.do")
public class LoginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从数据库拿值
        //连接数据库
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        LoginDao loginDao = new LoginDao();
        try {
            CustormerBean bean = loginDao.isLogin(username, password);
            if(bean.getUsername() == null && bean.getPassword() == null && bean.getEmail() == null
                    && bean.getUsername().length() == 0 && bean.getPassword().length() == 0 && bean.getEmail().length() == 0){
                resp.getWriter().write("账号或密码错误!");
            }else {
                resp.sendRedirect("/hello.html");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
