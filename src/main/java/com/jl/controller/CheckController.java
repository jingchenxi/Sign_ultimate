package com.jl.controller;
import com.alibaba.fastjson.JSON;
import com.jl.dao.CheckDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/check.do")
public class CheckController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        //判断账号存不存在
        String username = req.getParameter("username");
        CheckDao checkDao = new CheckDao();
        try {
            Object[] saveData = checkDao.isSaveData(username);
            System.out.println(saveData);
            Map<String,String> map = new HashMap<String, String>();
            System.out.println(map);
            if(saveData.length <= 0){
                resp.getWriter().write("不行重新来!");
            }else{
                String s = JSON.toJSONString(map);
                resp.getWriter().write(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
